export const state = () => ({
    popular: [],
    latest: [],
})

export const mutations = {
    setLatest(state, latest) {
        state.latest = latest
    },
    setPopular(state, popular) {
        state.popular = popular
    },
}

export const actions = {
   async nuxtServerInit({commit}) {
       const popular = await this.$axios.get(`https://api.bookdaddy.ru/popular`)
       commit('setPopular', popular.data)
       const latest = await this.$axios.get(`https://api.bookdaddy.ru/latest`)
       commit('setLatest', latest.data)
    },
}

export const getters = {
    popular: state => state.popular,
    latest: state => state.latest,
}

