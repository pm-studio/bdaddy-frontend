export const state = () => ({
    products: []
})

export const mutations = {
    setProducts(state, product) {
        state.products = products
    },
    setProduct(state, product) {
        state.products.unshift(product)
    }
}

export const actions = {
    async fetchProducts({commit}) {
        const products = await this.$axios.get(api, {
            params: {
                page: this.page,
                search: this.search
            }
        })
        commit('setProducts', products)
    },
    async fetchProductByID({commit}, productId) {
        try {
            const product = await this.$axios.$get(`https://api.bookdaddy.ru/product?id=${productId}`)
            commit('setProduct', product)
        } catch (e) {
            error(e)
        }
    }
}

export const getters = {
    products: state => state.products,
    getProductByID: state => (id) => {
        return state.products.find(product => product.id === parseInt(id))
    },
}
