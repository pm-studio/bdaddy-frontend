export const state = () => ({
    search: '',
    page: 1,
    last_page: 1000000,
    products: []
})

export const mutations = {
    setSearch(state, search) {
        state.search = search
    },
    setPage(state, page) {
        state.page = page
    },
    setLastPage(state, page) {
        state.last_page = page
    },
    setProducts(state, products) {
        state.products = state.products.concat(products)
    },
    resetProducts(state) {
        state.products = []
    }
}

export const actions = {
    async fetchProductBySearch({commit}, payload) {
        try {
            const products = await this.$axios.$get(`https://api.bookdaddy.ru/iproduct?page=${payload.page}&search=${payload.search}`)
            commit('setProducts', products)
        } catch (e) {
            error(e)
        }
    },
    setSearch({commit}, search) {
        commit('setSearch', search)
    },
    setPage({commit}, page) {
        commit('setPage', page)
    },
    setLastPage({commit}, page) {
        commit('setLastPage', page)
    },
    setProducts({commit}, products) {
        commit('setProducts', products)
    },
    resetProducts({commit}) {
        commit('resetProducts')
    }
}

export const getters = {
    search: state => state.search,
    page: state => state.page,
    last_page: state => state.last_page,
    products: state => state.products,
    getProductByID: state => (id) => {
        return state.products.find(product => product.id === parseInt(id))
    },
}
