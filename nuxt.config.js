export default {
    mode: 'universal',
    /*
    ** Headers of the page
    */
    head: {
        htmlAttrs: {
            lang: 'ru',
        },
        titleTemplate: '%s',
        title: 'Bookdaddy - сервис поиска книг по выгодным ценам',
        meta: [
            {charset: 'utf-8'},
            {name: 'viewport', content: 'width=device-width, initial-scale=1'},
            {hid: 'description', name: 'description', content: 'Bookdaddy - сервис поиска книг по выгодным ценам в интернет-магазинах'},
            {hid: 'keywords', name: 'keywords', content: 'поиск книг, поиск книги, найти книгу, книга, книги, поиск, цен, цена, сравнение цен, каталог, book, books, find, search, e-shopping, книги, магазин, литература, книжный магазин, интернет-магазин, торговля, книготорговля, электронная торговля, библиотека, электронная коммерция, издательство'}
        ],
        link: [
            { rel: 'icon', type: 'image/x-icon', href: 'https://bookdaddy.ru/favicon.ico' },
            { rel: 'stylesheet', href: 'https://fonts.googleapis.com/css?family=Roboto:100,300,400,500,700,900&amp;display=swap' }
        ]
    },
    /*
    ** Customize the progress-bar color
    */
    loading: {color: '#fff'},
    /*
    ** Global CSS
    */
    css: [],
    /*
    ** Plugins to load before mounting the App
    */
    plugins: [],
    /*
    ** Nuxt.js dev-modules
    */
    buildModules: [
        '@nuxtjs/vuetify',
        '@nuxtjs/pwa',
        ['@nuxtjs/google-analytics', {
            id: 'UA-166339570-1'
        }]
    ],
    vuetify: {
        defaultAssets: false,
    },
    pwa: {
        manifest: {
            name: 'Bookdaddy',
            short_name: "Bookdaddy",
            lang: 'ru',
        }
    },
    /*
    ** Nuxt.js modules
    */
    modules: [
        // Doc: https://axios.nuxtjs.org/usage
        '@nuxtjs/axios',
        [
            '@nuxtjs/yandex-metrika',
            {
                id: '62697997',
                webvisor: true,
                clickmap:true,
                useCDN:false,
                trackLinks:true,
                accurateTrackBounce:true,
            }
        ],
    ],
    /*
    ** Axios module configuration
    ** See https://axios.nuxtjs.org/options
    */
    axios: {},
    /*
    ** Build configuration
    */
    build: {
        extractCSS: {
            ignoreOrder: true
        },
        optimisation: {
            splitChunks: true,
        },
        /*
        ** You can extend webpack config here
        */
        extend(config, ctx) {
        },
    }
}
