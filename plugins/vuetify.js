import Vue from 'vue'
import Vuetify from 'vuetify/lib'
//import colors from 'vuetify/es5/util/colors'
import minifyTheme from 'minify-css-string'

Vue.use(Vuetify, {
    icons: {
        iconfont: 'mdiSvg',
    },
    breakpoint: {
        thresholds: {
            xs: 576,
            sm: 768,
            md: 992,
            lg: 1200,
        },
        scrollBarWidth: 24,
    },
    customVariables: ['~/assets/variables.scss'],
    theme: {
        dark: false,
/*        primary: '#121212', // a color that is not in the material colors palette
        accent: colors.grey.darken3,
        secondary: colors.amber.darken3,
        info: colors.teal.lighten1,
        warning: colors.amber.base,
        error: colors.deepOrange.accent4,
        success: colors.green.accent3,*/
        options: {
            minifyTheme,
            themeCache: {
                get: key => localStorage.getItem(key),
                set: (key, value) => localStorage.setItem(key, value),
            },
        },
    },
})
